# Starfinder

The goal of this project is to create a webapp in Golang to maintain character information for characters in a Starfinder game. 

Resources:
    https://thetrove.net/Books/Starfinder/Core/
    
    https://www.aonsrd.com/
    https://www.starjammersrd.com/